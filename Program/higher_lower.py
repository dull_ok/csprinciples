from random import randint
from gasp.utils import read_yesorno

num = randint(1, 100)

while True:
    guess = int(input("Guess the number I am thinking of that is between 1 and 100. "))
    if guess == num:
        print("Your right!!!")
        if read_yesorno("would you like another game? "):
            print("Ok!")
        else:
            break
    elif guess > num:
        print("Lower ")
    elif guess < num:
        print("Higher ")
