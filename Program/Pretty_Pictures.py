from gasp import *
from random import randint

def draw_core(x,y):
    for r in 20, 60, 90, 150:
        Circle((x, y), r)
    Line((x-50, y+35), (x+50, y+35))
    Line((x-50, y-35), (x+50, y-35))
    Line((x, y-10), (x, y+10))
    Arc((x, y+105), 115, 1, 180)
    Arc((x, y-105), 115, 359, 180)

begin_graphics()
x = randint(150, 450)
y = randint(200, 300)
draw_core(x, y)
update_when('key_pressed')
end_graphics()
