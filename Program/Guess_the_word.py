import random
words = ["port-eighty", "shared-secret", "cybersecurity", "socket", "buffering", "packet-vortex"]

def getRandomWord(wordList):
    wordIndex = random.randint(0, len(wordList) - 1)
    return wordList[wordIndex]

def displayBoard(missedLetters, correctLetters, secretWord):
    print(len(missedLetters))

    print('Missed letters:', end=' ')
    for letter in missedLetters:
        print(letter, end=' ')

    blanks = '_' * len(secretWord)

    for i in range(len(secretWord)):
        if secretWord[i] in correctLetters:
            blanks = blanks[:i] + secretWord[i] + blanks[i+1:]

        for letter in blanks:
            print(letter, end=' ')

def getGuess(alreadyGuessed):
    while True:
        print("Guess a letter.")
        guess = input()
        guess = guess.lower()
        if len(guess) != 1:
            print("Please enter a single letter.")
        elif guess in alreadyGuessed:
            print("You have already guessed that letter. Choose again.")
        elif guess not in "abcdefghijklmnopqrstuvwxyz-":
            print("Please enter a letter or a dash.")
        else:
            return guess

print("Guess the word")
missedLetters = ''
correctLetters = ''
secretWord = getRandomWord(words)

while True:
    displayBoard(missedLetters, correctLetters, secretWord)
    guess = getGuess(missedLetters + correctLetters)
    
    if guess in secretWord:
        correctLetters = correctLetters + guess
        foundAllLetters = True
        for i in range(len(secretWord)):
            if secretWord[i] not in correctLetters:
                foundAllLetters = False
                break
        if foundAllLetters:
            print("Yes! The secret word is " + secretWord + "! You have won!")
            break
        else:
            missedLetters = missedLetters + guess
            
            if len(missedLetters) >= 5:
                displayBoard(missedLetters, correctLetters, secretWord)
                print("You have run out of guesses!\nAfter " + str(len(missedLetters)) + " missed guesses and " + str(len(correctLetters)) + " correct guesses, the word was " + secretWord)
                break
