def count_digit(digit, number):
    count = 0

    while digit > 0:
        n = digit % 10
        if n == number:
            count += 1
        digit = digit // 10

    print(count)

print(count_digit(1565, 5))
