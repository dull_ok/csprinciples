def sean(numbers):
    """
      >>> sean([1, 2, 3])
      2
      >>> sean([15, 10, 5])
      10
    """
    high = numbers[0] 
    low =  numbers[0]

    for i in numbers:
        if not high or high < i:
            high = i
        elif not i or low > i:
            low = i
    else:
        median = high - low
        return median

if __name__ == '__main__':
    import doctest
    doctest.testmod()
