import random
import sys 


def print_table(table_letter, people):
    peeps = ""
    for person in people[:-1]:
        peeps += f"{person}, "
    peeps += f"and {people[-1]}."
    print(f"Table {table_letter} has:\n    {peeps}\n")


def form_groups():
    people = [
        'Gabriel', 'Eimi', 'Mayah', 'Anupama', 'Grant', 'Lary', 'Toby',
        'Kellan', 'Rockwell', 'Ronan', 'Emory', 'Keirsten', 'Evan', 'Noah',
        'Blu', 'Jake', 'Conrad', 'Xander', 'Christian', 'Jack', 'Alex', 'Sean'
    ]

    left_overs = len(people) % 4
    group = 'A'
    while left_overs != 0:
        table = []
        for count in range(3):
                person = random.choice(people)
                table.append(person)
                people.remove(person)
        print_table(group, table)
        group = chr(ord(group) + 1)
        left_overs = len(people) % 4
    while people:
        table = []
        for count in range(4):
            person = random.choice(people)
            table.append(person)
            people.remove(person)
        print_table(group, table)
        group = chr(ord(group) + 1)
            
if __name__ == '__main__':
    form_groups() 
