"""
  >>> type(this)
  <class 'str'>
  >>> type(that)
  <class 'int'>
  >>> type(something)
  <class 'float'>
"""
this = "hi"
that = 1
something = 1.2

if __name__ == '__main__':
    import doctest
    doctest.testmod()

