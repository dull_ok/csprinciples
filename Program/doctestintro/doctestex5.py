"""
  >>> type(thing1)
  <class 'float'>
  >>> type(thing2)
  <class 'int'>
  >>> type(thing3)
  <class 'str'>
"""
thing1 = 1.2
thing2 = 1
thing3 = "hi"

if __name__ == '__main__':
    import doctest
    doctest.testmod()
