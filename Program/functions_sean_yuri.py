def total(thing):
    """
        >>> total(3)
        10
        >>> total(7)
        21
        >>> total(10)
        29
    """

    total = thing * 11 // 4 + 2
    return total

def guess(x, y, z):
    """
        >>> guess(14, 17, 15)
        225
        >>> guess(55, 61, 49)
        2401
        >>> guess(13, 14, 12)
        144
    """

    return z ** 2


if __name__ == '__main__':
    import doctest
    doctest.testmod()

