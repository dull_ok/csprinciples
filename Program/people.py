class Person:
    def __init__(self, name="Sean", age=16):
        self.name = name
        self.age = age

    def about(self):
        print(f"I am {self.name} and I am {self.age} year old.")
