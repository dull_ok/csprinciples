sentence = str(input("Enter a sentence: "))
a_count = 0

for letter in sentence:
    if letter == "a":
        a_count += 1
if a_count > 0:
    print('The letter "a" appears in your sentence ' + str(a_count) + ' times.')
else:
    print('The letter "a" does not appear in your sentence.')

