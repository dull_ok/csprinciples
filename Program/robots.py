from gasp import *          # So that you can draw things
from random import randint       

def place_player():
    global player_x, player_y, player_shape
    player_x = randint(0, 63)
    player_y = randint(0, 47)
    player_shape = Circle((10 * player_x + 5, 10 * player_y + 5), 5, filled=True)

def safely_place_player():
    global player_x, player_y, robot_x, robot_y
    place_player()

    if robot_x == player_x and robot_y == player_y:
        place_player()

def move_player():
    global player_x, player_y, player_shape

    key = update_when('key_pressed')

    if key == 'w' and player_y < 47:
        player_y += 1
    if key == 'x' and player_y > 0:
        player_y -= 1
    if key == 'd' and player_x < 63:
        player_x += 1
    if key == 'a' and player_x > 0:
        player_x -= 1
    if key == 'q' and player_y < 47 and player_x > 0:
        player_y += 1
        player_x -= 1
    if key == 'e' and player_y < 47 and player_x < 63:
        player_y += 1
        player_x += 1
    if key == 'z' and player_y > 0 and player_x > 0:
        player_y -= 1
        player_x -= 1
    if key == 'c' and player_y > 0 and player_x < 63:
        player_y -= 1
        player_x += 1
    if key == 's':
        player_x = randint(0, 63)
        player_y = randint(0, 47)
    move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))

def place_robot():
    global robot_x, robot_y, robot_shape
    
    robot_x = randint(0, 63)
    robot_y = randint(0, 47)
    robot_shape = Box((10 * robot_x, 10 * robot_y), 10, 10, filled=True)

def move_robot():
    global player_x, player_y, robot_x, robot_y, robot_shape
    
    if robot_y < player_y:
        robot_y += 1
    if robot_y > player_y:
        robot_y -= 1
    if robot_x < player_x:
        robot_x += 1
    if robot_x > player_x:
        robot_x -= 1
    move_to(robot_shape, (10 * robot_x, 10 * robot_y))

def check_collisions():
    global finished
    
    if robot_x == player_x and robot_y == player_y:
        Text("You have been caught!!!", (320, 240), size=50)
        finished = False
        sleep(3)

begin_graphics()
global finished

finished = True

place_robot()
safely_place_player()

while finished:
    move_player()
    move_robot()
    check_collisions()

end_graphics()
