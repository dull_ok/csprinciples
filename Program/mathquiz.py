from random import randint
from os import system

system("clear")
correct = 0

qnum = int(input("How many questions do you want? "))

for q in range(qnum):
    num1 = randint(1, 10)
    num2 = randint(1, 10)
    question = f"what is {num1} times {num2}? "
    answer = num1 * num2
    response = int(input(question))

    if answer == response:
        print("That's right.")
        correct += 1
    else:
        print(f"No, the answer is {answer}.")

print(f"I asked you {qnum} questions. You got {correct} of them right.") 
if correct == qnum:
    print(" __     |\  _ |\ \n(_  | | |/ |_ |/ \n__) \_/ |  |_ |\ ")
elif correct == 0:
    print("You aren't the brightist are you?")
else:
    print("Well done.")
