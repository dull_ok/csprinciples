Group C Presentation Notes:

1. Understand the instructions.
2. Study the 5 Big Ideas.
3. When taking a pratice test if you don't understand a question skip it and go back to it later.
4. Take pritice tests and study resouces like Khan Academy, College Board, and Test Guide's guide to the APCSP exam.

I personaly think the three resources presented are all very useful.


Practice Questions:

Source: Khan Academy
1. Which statement is true about packet loss on the Internet?

- [ ] As long as two computers are using the Transmission Control Protocol (TCP), the packets that they send to each other can never be lost.
- [ ] As long as two computers are using the Internet Protocol (IP), the packets that they send to each other can never be lost.
- [ ] When two computer are using the Internet Protocol (IP), the packets that they send to each other can be lost along the way, but they can be recovered via retransmission.
- [x] When two computer are using the Transmission Control Protocol (TCP), the packets that they send to each other can be lost along the way, but they can be recovered via retransmission.

2. Computer A wants to send data to Computer B using the TCP/IP protocols.

- [x] Computer A splits the data into packets. It does a three way handshake with Computer B to establish the connection, then sends each packet to the nearest router. If it detects packet loss, it re-sends the missing packets.
- [ ] Computer A splits the data into packets. It looks up the best routing path in a routing table, records the path in the packets, and sends it to the first router in the path. The packet follows the path until it gets to the final destination.
- [ ] Computer A splits the message into packets. It sends each packet to Computer B as fast as it can, and then closes the connection.
- [ ] Computer A creates two packets, one with data, and the other with the metadata. Computer A first sends along the metadata and then sends along the actual data.

3. One problem with packet-based messaging systems like the Internet Protocol is that packets might arrive out of order. TCP is a protocol that can deal with out-of-order packets. When used over IP, each IP packet contains a TCP segment with metadata and data. Which part of the packet contains the sequencing information that's used by TCP to reassemble packets?

- [ ] IP metadata
- [x] TCP metadata
- [ ] TCP data
- [ ] None of the above

