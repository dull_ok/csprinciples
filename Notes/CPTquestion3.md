# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
    Make a game where robots chase you and in order to win,
    you have to make them crash into each other.

2. Describes what functionality of the program is demonstrated in the
   video.
    The player is getting chased by the robots,
    The player makes the robots crash into each other and the player wins,
    The player gets surronded by the robots and the player gets chaght.

3. Describes the input and output of the program demonstrated in the
   video.
    Input: A key is entered

    Output: The player moves

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
    
    while not game_over:
    finished = False
    junk = []

2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.

    for robot in robots:
        if collided(robot, junk):
            continue

        jbot = robot_crashed(robot)

        if not jbot:
            surviving_robots.append(robot)
        else:
            remove_from_screen(jbot.shape)
            jbot.junk = True
            jbot.shape = Box(
                (10 * jbot.x, 10 * jbot.y), 10, 10, filled=True)
            junk.append(jbot)

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
    The name of the list being used is called "junk".

2. Describes what the data contained in the list represent in your
   program
    The data contained in the the list represents all the defeated robots.

3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
    You could not write the program differently because the program couldn't tell if something collided or not with the junk and you could not win.

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
    def robot_crashed(the_bot):
    for a_bot in robots:
        if a_bot == the_bot:
            return False
        if a_bot.x == the_bot.x and a_bot.y == the_bot.y:
            return a_bot
    return False

2. The second program code segment must show where your student-developed
   procedure is being called in your program.

    for robot in robots:
        if collided(robot, junk):
            continue

        jbot = robot_crashed(robot)

        if not jbot:
            surviving_robots.append(robot)
        else:
            remove_from_screen(jbot.shape)
            jbot.junk = True
            jbot.shape = Box(
                (10 * jbot.x, 10 * jbot.y), 10, 10, filled=True)
            junk.append(jbot)
    
    robots = []

    for robot in surviving_robots:
        if not collided(robot, junk):
            robots.append(robot)

    if not robots:
        finished = True
        Text("You win!", (160, 240), size=36)
        sleep(3)
        return

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
    It detects if the robot collided with another robot or not and decides if you have won yet.
