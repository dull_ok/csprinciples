# Repeating Steps with Strings

- new_string_b = new_string_b + letter (prints the phrase normaly)
- new_string_a = letter + new_string_a (prints the phrase in reverse)
- new_string = letter + new_string + letter (prints the phrase mirrored)

example of a incoded measage
"""
message = "meet me at midnight"
a_str = "abcdefghijklmnopqrstuvwxyz. "
e_str = "zyxwvutsrqponmlkjihgfedcba ."
encoded_message = ""
for letter in message:
    pos = a_str.find(letter)
    encoded_message = encoded_message + e_str[pos]
print(encoded_message)
"""

- Accumulator Pattern (The accumulator pattern is a set of steps that processes a list of values)
- Palindrome (A palindrome has the same letters if you read it from left to right as it does if you read it from right to left)
- String (A string is a collection of letters, numbers, and other characters like spaces inside of a pair of single or double quotes)
