# Bitwise Math

- Bitwise operations = affect one or more bits in a binary value. 
- Unsigned = a variable that can only hold positive numbers.

- Computers use binary to represent numbers
- Depending on the programming language and the type of numbers the bitwise operations may have different results.
- Python unlike most programming languages it can store negative values.

# Number Representations

- Byte = a group of 8-bits

- C language has four different integer types:
    - char
    - short
    - int
    - long

## Representing Negative Numbers

- Sign Magitude Integers(SMT) uses the left-most bit to indicate positive or negative.
    - 1 = negative
    - 0 = positive
- SMT has trouble adding and has an ambiguous zero.

- One's Compliment(OC) is the same as SMT but instead it stores negative numbers are stored as their complement(their not value) so it basicly flips all the 1s and 0s to show a negative.
- 8-bit range from -127 to 127.
- Still has an ambiguous zero.

- Two's Compliment(TC) is also the same as SMT but instead stores negative numbers as their complement plus 1. 
- 8-bit range from -128 to 127.

## Floating-Point Representation

- Floating point numbers are approximations.
- IEEE 754 standard defines floating point numbers.

- Floating Point is made of three parts:
    - Sign
    - Exponent
    - Mantissa
- IEEE 754 standard defines multiple precisions and the most common are:
    - Single which is made of 32 bits:
        - Sign bit = 0 for postitive or 1 for negative
        - 8 exponent bits = an unsigned value
        - 23 mantissa bits = fraction bits
    - Double which is made of 64 bits:
        - Sign bit
        - 11 exponent bits
        - 52 mantissa bits

## Fixed-Point Representation

- Fixed point values stor digits to specified precision.

