# Binary, Bytes, and Bitwise Operators in Python (Overview)

- CPUs are built on transistors which are tiny componets that are on or off and every piece of data are represented through a series of transistors in a pattern of on and off forming a binary number.
- Bitwise operations are used to combine or change one or more binary numbers:

## Operators
- & = Bitwise AND
- | = Bitwise OR
- ^ = Bitwise XOR
- ~ = Bitwise NOT
- << = Shift Bits Left
- >> = Shift Bits Right

## Bitwise AND Operator
- If both are true it is TRUE
- If one is true and one if false it is FALSE
- If both are false it is FALSE
- Interger Form = 24 & 12 --> 8
- Bit Form = 11000 & 01100 --> 01000

## Bitwise OR Operator
- If both are true it is TRUE
- If one is true and one if false it is TRUE
- If both are false it is FALSE
- Interger Form 24 | 12 --> 28
- Bit Form = 11000 | 01100 --> 11100

## Bitwise XOR Operator
- If one is true and one is false it is TRUE
- If both are true it is FALSE
- If both are false then it is FALSE
- Interger Form = 24 ^ 12 --> 20
- Bit Form = 11000 ^ 01100 --> 10100

## Bitwise NOT Operator
- Turns a interger into it oppiste and subtracts one from the number.
- ~ 1 --> -2
- ~ -2 --> 1

## Bitwise SHIFT Operators
- Shifts the bits to the left or right
- Interger Form = 1 << 1 --> 2
- Bit Form = 00001 << 00001 --> 00010
- Interger Form = 14 >> 1 --> 7
- Bit Form = 01110 >> 00001 --> 00111

# Binary Numbers

- Transistor works when an electrical charge is applied to it base the current can flow through the transistor between the collector and the emitter.
- In Base N no digit can be larger than n.


