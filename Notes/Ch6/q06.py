def move(Turtle, size, angle):
    Turtle.forward(size)
    Turtle.right(angle)
    Turtle.forward(size)
    Turtle.right(angle)
    Turtle.forward(size)
    Turtle.right(angle)
    Turtle.forward(size)
    Turtle.right(angle)

from turtle import *
space = Screen()
t = Turtle()
move(t, 100, 90)
