def total_price(miles, miles_per_gallon, price_per_gallon):
    num_gallons = miles / miles_per_gallon
    total = num_gallons * price_per_gallon
    return total

print(total_price(500, 26, 3.45))
