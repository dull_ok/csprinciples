def square(Turtle, size):
    Turtle.forward(size)
    Turtle.right(90)
    Turtle.forward(size)
    Turtle.right(90)
    Turtle.forward(size)
    Turtle.right(90)
    Turtle.forward(size)
    Turtle.right(90)


from turtle import *    # use the turtle library
space = Screen()        # create a turtle screen (space)
Malik = Turtle()        # create a turtle named malik
square(Malik, 100)      # draw a square of size 100
square(Malik, 75)       # draw a square of size 75
square(Malik, 50)       # draw a square of size 50
square(Malik, 25)       # draw a square of size 25
