Set Up

1. Install GIT
2. Type: git clone (repository URL)
3. Type: python3 -m pip install gasp
4. Download .vimrc as it is
5. Rename .vimrc.txt to .vimrc
6. Move .vimrc to home directory
