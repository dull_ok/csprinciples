asymmetric ciphers - a process that uses one public key and one private key to hide and reveal infomation. 

authentication - showing or proving something is true or valid.

bias - having favor over one thing or another.

Certificate Authority (CA) - a trusted third party that issues certificates.

citizen science - the collection and analysis of information that relates to members of the general public.

Creative Commons licensing - a non-profit organisation that issues out free licences for people to use when making their work public.

crowdsourcing - obtainging information from a large number of people pain or unpaid.

cybersecurity - security for your online data.

data mining - analyzing databasses in order to make new information.

decryption - reveal information.

digital divide - the gap between those who have access to the internet and those who do not.

encryption - hide information.

intellectual property (IP) - work that someone has the rights for and can copyright, tradmark, and etc.

keylogging - the recording of every key pressed by the user.

malware - software that is designed to damage or gain access to a computer system.

multifactor authentication - a multilayered security system for data.

open access - the unrestricted right to use or benefit from something.

open source - original source code that is made freely to be used and modified.

free software - software that gives the users freedom to do whatever they like with it.

FOSS - Free and Open Source Software.

personally identifiable information (PII) - any information that can be used to identitfy an individual.

phishing - the practice of tricking people online to access private information.

plagiarism - using someones work without giving credit.

public key encryption - a method of hiding data or signing data with one public key and one private key.

rogue access point - a access point inserted into a network without the owner's permission.

targeted marketing - a approach to increase awareness for a service or product to a specific audiences.

virus - a computer program the infects a computer and corrupts or deletes data without permission or the knowledge of the user.
