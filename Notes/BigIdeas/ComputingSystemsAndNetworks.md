Bandwidth - The maximum amount of the data that can be transmitted over an internet connection in a given amount of time.

Computing device - a device that can perform substantial computations without human intervention.

Computing network - interconnected computing devices that can share and exchange data with each other.

Computing system - intergrated devices that input, output, process, and store data.

Data stream - the process of transmitting a continuous flow of data.

Distributed computing system - the method of making multiple computing work together.

Fault-tolerant - the ability of a sytem to continue running without problems when one or more of its components fail.

Hypertext Transfer Protocol (HTTP) - an application protocol for distributed, collaborative, hypermedia information systems that allow users to communicate data on the world wide web.

Hypertext Transfer Protocol Secure (HTTPS) - a protocol that secures communication and data transfer between a user's web browser and a website. (secure version of HTTP)

Internet Protocol (IP) address - a series of numbers that identifies any device on a network. 

Packets - a small segment of a larger piece data.

Parallel computing system - the study, design, and implementation of algorithms in a way as to make use of multiple processors to solve a problem.

Protocols - a standardized set of rules for formatting and processing data.

Redundancy - a system design in which a component is duplicated so in case one of them fails there is a backup.

Router - a computer that is a gateway between two networks at OSI layer 3 and that relays and directs data packets through that inter-network.
