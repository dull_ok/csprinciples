abstraction - the process of removing parts of a code or program that aren't needed or distract from more important parts.

analog data - data that is represented in a physical way.

bias - computer systems that unfairly discriminate against certin individuals or groups of individuals in favor of others.

binary number system - a numbering scheme in which there are only two possible values 1 and 0.

bit - the smallest increment of data on a computer.

byte - a unit of data that is eight binary data long.

classifying data - the process of analyzing structred or unstructured data and organizing into categories based on different types of metadata.

cleaning data - the process of fixing or removing incorrect, currupted, incorrectly formatted, duplicate, or incomplete data within a dataset.

digital data - the electronic representationn of information in a format or language that machines can understand.

filtering data - the processmof choosing a smaller part of your data set and using that subset for veiwing or analysis.

information - data

lossless data compression - a compression technique that does not lose any data in the compression process.

lossy data compression - a compression technique that does not lose any data in the compression process.

metadata - data that provide information about other data.

overflow error - when the data type used to store data was not large enough to hold the data.

patterns in data - recurring sequences of data over time that can be used to predict trends.

round-off or rounding error - the diffence between an approximation of a number used in computation and its exact value.

scalability - the measure of a system's ability to increase or decrease in proformance and cost in response to changes in application and system processing demands.
