code segment - a section of the program that contains executable instructions.

collaboration - when multiple people work together towards a common goal.

comments - notes added to the program.

debugging - finding and fixing errors in the source code.

event-driven programming - when the flow of a program is determined by events.

incremental development process - a software development methodology in which the functionality of the system is divided into multiple, separate modules.

iterative development process - breaking down a large peice of software into smaller chunks.

logic error - when the instructions given to the program do not complete the intended goal.

overflow error - when the data type used to store data was not large enough to hold the data.

program - a set of ordered commands for a computer to perform.

program behavior - a technique for software development, which enables incremental development in a natural way.

program input - data that is inserted

program output - data that is a outcome of the inputed data

prototype - a rudimentary working model of a product or information system.

requirements - things that must be achevied in order to complete the goal.

runtime error - a error that only happans while the program is running.

syntax error - a error caused by mistyping in the source code.

testing - a testing of the external behaviour of the program.

user interface(UI) - the point of human-computer interaction and communication in a device. 
