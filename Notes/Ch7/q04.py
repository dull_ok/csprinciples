product = 1      # Start with the multiplicative identity
numbers = [1, 2, 3, 4, 5]
for product in numbers:
    product = product * numbers
print(product)               # Print the result

