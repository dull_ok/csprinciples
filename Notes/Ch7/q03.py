def sum_list(num_list):
    total = 1               # Start with additive identity
    for n in num_list:             # Loop through each number
        total = total * n         # Accumlate the total
    return total

my_nums = (2, 5)                # Create a list of numbers
print(sum_list(my_nums))          # Print the call result to test

