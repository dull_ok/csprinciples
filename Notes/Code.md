# Saving Files
- git add .
- git commit -m "FILE"
- git commit -am "FILE" = used to update a file already exsits and makes it so you don't have to do "git add ." first
- git push

# File Stuff
- vi "FILE".md = makes a markdown file which helps make your notes look nice and organize them
- vi "FILE".py = the space where you develop your program 
- mkdir = makes a directory
- mv "FILE" "..." = rename a file or move it to a diffrent directory 

# Symbols
- = / == (equal to)
- != (not equal to)
- + (addition)
- - (subtraction)
- * (multiplcation)
- / (division)
- // (floor division / divides two numbers and rounds down to the nearest integer))
- % (moldulo operator / divides two numbers and returns the remainder)
- > (greater than)
- < (less than)
- || (returns boolean value true if either or both operands is true and returns false otherwise)

# Python3
- Python3 = enters Python3
- Go to Ch5 in notes for more about turtles

# Imports
- gasp (graghics)
- random 

# Doc Test Notes
example(
if __name__ == "__main__":
    import doctest
    doctest.testmod()
)example end
