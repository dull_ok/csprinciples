# Chapter 5

- from turtle import * = importes turtle from libary 
- space = Screen() = creates a turtle space
- "NAME" = Turtle = creates a turtle and names it also when turtle is created it will face to the east
- Turtle.forward/backward/right/left() = makes turtle move forward, backward, right, or left,
- penup/pendown() = moves your pen up or down
- goto(x,y) = moves your turtle to a specific coordinate
- Turtle.pensize() = changes the turtles pen size
- Turtle.color('COLOR') = changes the turtles pen color

