from turtle import *      
space = Screen()          
sean = Turtle()            

# Make a square
sean.forward(100)          
sean.right(90)             
sean.forward(100)          
sean.right(90)             
sean.forward(100)          
sean.right(90)             
sean.forward(100)          

# Position for roof
sean.right(90)

# Make a roof
sean.forward(100)
sean.right(-120)
sean.forward(100)
sean.right(-120)
sean.forward(40)
sean.right(150)
sean.forward(50)
sean.left(90)
sean.forward(15)
sean.left(90)
sean.forward(75)
sean.right(30)
sean.forward(30)
sean.penup()

#window
sean.left(120)
sean.forward(40)
sean.left(90)
sean.forward(20)
sean.pendown()
sean.forward(20)
sean.right(90)
sean.forward(20)
sean.right(90)
sean.forward(20)
sean.right(90)
sean.forward(20)
sean.right(90)
sean.forward(10)
sean.right(90)
sean.forward(20)
sean.right(90)
sean.forward(10)
sean.right(90)
sean.forward(10)
sean.right(90)
sean.forward(20)
sean.right(180)

#door
sean.penup()
sean.forward(70)
sean.right(90)
sean.pendown()
sean.backward(15)
sean.forward(30)
sean.left(90)
sean.forward(50)
sean.left(90)
sean.forward(30)
sean.left(90)
sean.forward(50)
sean.backward(25)
sean.penup()
sean.left(90)
sean.forward(10)
sean.pendown()
sean.forward(2)


