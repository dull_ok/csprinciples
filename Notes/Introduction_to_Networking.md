# Chapter 1 Introduction

- Back in the day telephones and computers worked by having a wire connecting a telephone or computer to a central office that would connect your wire to the person you are trying to contacts wire.
- Leased Lines = A always on connection that would connect computers to each other that could be leased.
- Store-and-Forward network = a method that would work by sending the data to other computers along that the way that would store and foward your message to the person you meant to send too. The main downside to this method is that it could take from minutes to hours and even days to send your message.
- Packets are the smaller pieces of a larger piece of data that cut up so it is easier to tranfer and did not make the computer require as much computing power to send and recieve.
- Routers = also known as "Interface Message Processors" (IMPs) are specialized computers for moving packets and this ultimatly became the more widly used method over Store-and-Forward method.
- "Local Area Network" (LAN)
- "Wide Area Network" (WAN)
- Address = a unique name or number for a computer.
- Hop = A single physical network connection.

# Chapter 2 Network Architecture 

- The people who first built the internet broke the overall problem into four basic subproblems Application, Transport, Internetwork, and Link.
- The Link Layer = the layer responsible for connecting your devices to its local network and moving the data across a single hop.
- "Carrier Sense Multiple Access with Collision Detection" (CSMA/CD) = a method that when you want to send data your device firsts listens to see if another device is already sending data on the network. If no other devices are sending data your device starts sending its data. As your device is sending data it also listens to see if it can receive its own data. If your device receives its own data, it knows that the channel is still clear and continues transmitting. But if two devices started sending at about the same time, the data collides, and your device does not receive its own data. When a collision is detected, both devices stop transmitting, wait a bit, and retry the transmission. 
- The Internetwork Layer (IP) = the layer that addresses and firgures out how to best move your packets towards its destination.
- "Internet Protocol" (IP)
- The Transport Layer (TCP) = the layer responsible for packets if they get badly delayed, lost, recived out of order, and etc. 
- "Transport Control Protocol" (TCP)
- The Application Layer = the application.
- Fiber Optic = A data transmission technology that encodes data using light and sends the light down a very long strand of thin glass or plastic.
- offset = The relative position of a packet within an overall message or stream of data.
- Window Size = The amount of data that the sending device is allowed to send before waiting for an acknowledgement. 

# Chapter 3 Link Layer

- MAC Address = A 48-bit serial given to every device that is manufactured.
- "Media Access Control" (MAC)
- Token = A method to allow many devices share the same physical media without collisions. It does this by making each device have to receive a token before it send its data.
- Base Station = The first router that handles your packets.
- Broadcast = Sending a packet in a way that all the stations connected to a (LAN) will receive the packet.
- Gateway = A router that connects a (LAN) to a (WAN). Devices that want to send data to (WAN) must send their packets to the gatway for forwarding. 

# Chapter 4 Internetworking Layer

- IPv4 Address = A adress that is made up of four numbers that can only range from 0 to 255 and are seprated by dots.
- IPv6 Address = The 128 bit version of IPv4 and that uses hex.
- "Dynamic Host Configuration Protocol" (DHCP) = How a portable computer gets an IP address when it is moved to a new location.
- Host Identifier = The portion of an IP address that is used to identify a computer within a local area network.
- "Network Address Translation" (NAT) = A method that allows a single global IP address to be shared by many computers on a single local area network.
- Network Number = The portion of an IP address that is used to identify which local network the computer is connected to.
- Packet Vortex = An error situation where a packet gets into an infinite loop because of errors in routing tables.
- "Regional Internet Registry" (RIR) = The five RIRs roughly correspond to the continents of the world and allocate IP address for the mojor geographical areas of the world.
- Routing Tables = Information maintained by each router that keeps track of which outbound link should be used for each network number.
- "Time To Live" (TTL) = A number that is stored in every packet that is reduced by one as the packet passes through each router. When the TTL reaches zero, the packet is discarded.
- Traceroute = A command that is available on many Linux/UNIX systems that attempts to map the path taken by a packet as it moves from its source to its destination.

# Chapter 5 The Domain Name System (DNS)

- Domain Name System" (DNS) = A system of protocols and servers that allow networked applications to look up domain names and retrieve the corresponding IP address for the domain name.
- Domain Name = A name that is assigned within a top-level domain.
- "International Corporation for Assigned Network Names and Numbers" (ICANN) = Assigns and manages the top-level domains for the Internet.
- Subdomain = A name that is created “below” a domain name. For example, “umich.edu” is a domain name and both “www.umich.edu” and “mail.umich.edu” are subdomains within “umich.edu”.
- "Top Level Domain" (TLD) = The rightmost portion of the domain name. Example TLDs include “.com”, “.org”, and “.ru”, “.club” and “.help”.
- The port number for the Domain Name System is port 53.

# Chapter 6 Transport Layer

- Buffering = Temporarily holding on to data that has been sent or received until the computer is sure the data is no longer needed.
- Port = A way to allow many different server applications to be waiting for incoming connections on a single computer. Each application listens on a different port. Client applications make connections to well-known port numbers to make sure they are talking to the correct server application.

# Chapter 7 Application Layer

- "HyperText Markup Language" (HTML) = A textual format that marks up text using tags surrounded by less-than and greater-than characters.
- "HyperText Transport Protocol" (HTTP) = An Application layer protocol that allows web browsers to retrieve web documents from web servers.
- HTTP is port 80.
- "Internet Message Access Protocol" (IMAP) = A protocol that allows mail clients to log into and retrieve mail from IMAP-enabled mail servers.
- The port number for the IMAP mail retrieval protocol is port 143.
- Flow Control = When a sending computer slows down to make sure that it does not overwhelm either the network or the destination computer. Flow control also causes the sending computer to increase the speed at which data is sent when it is sure that the network and destination computer can handle the faster data rates.
- Socket = A software library available in many programming languages that makes creating a network connection and exchanging data nearly as easy as opening and reading a file on your computer.
- Status Code = One aspect of the HTTP protocol that indicates the overall success or failure of a request for a document. The most well-known HTTP status code is “404”, which is how an HTTP server tells an HTTP client (i.e., a browser) that it the requested document could not be found.
- Telnet = A simple client application that makes TCP connections to various address/port combinations and allows typed data to be sent across the connection.
- The most widely used document in aplication layer protocols are RFC.

# Chapter 8 Secure Transport Layer

- Certificate Authority = An organization that digitally signs public keys after verifying that the name listed in the public key is actually the person or organization in possession of the public key.
- Ciphertext = A scrambled version of a message that cannot be read without knowing the decryption key and technique.
- Private Key = The portion of a key pair that is used to decrypt transmissions.
- Public Key = The portion of a key pair that is used to encrypt transmissions.
- asymmetric key = An approach to encryption where one (public) key is used to encrypt data prior to transmission and a different (private) key is used to decrypt data once it is received.
- Shared Secret = An approach to encryption that uses the same key for encryption and decryption.
- "Secure Sockets Layer" (SSL) = An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Transport Layer Security (TLS).
- "Transport Layer Security" (TLS) = An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Secure Sockets Layer (SSL).

