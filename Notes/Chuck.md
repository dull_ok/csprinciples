1. The HTTP request/response cycle
When the server receives that request, it uses the information included in the request to build a response that contains the requested information. Once built, that response is sent back to the client in the requested format, to be rendered to the user.

2. What a socket is
One endpoint of a two way communication link between two programs.

3. The RFC process and the democracy of the web
RFC = A Request for Comments
RFC is the call of a fuction module.
The Democracy of the web uses information and communication technology in political and governance processes.

4. The parts of a Uniform Resource Locator (URL)
Scheme example = 
Subdomain = 
Domain = 
Top level domain = 
Port number = 
Path =

5. The most common TCP port numbers
Most common ports 80/443
Port 80 = HTTP
Port 443 = HTTPS
Port 20/21 = FTP
Port 22 = SSH
Port 25 = SMTP
Port 53 = DNS
Port 123 = NTP
Port 179 = BGP
Port 500 = IPsec
Port 587 = SMTP
Port 3389 = RDP
