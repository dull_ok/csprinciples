# CPT Scoring Guidelines

Program Purpose and Function - the video: shows your program functionioning and the written response: describes how your program functions.

Data Abstraction - the written response: shows all the data.

Managing Complexity - the written response: shows a list being used to manage complexity and explains how it works and how it could or couldn't be made without using list.

Procedural Abstraction - the written response: shows a student-developed procedure using a parameter and how it works.

Algorithm Implementation - the written response: shows a student developed algorithm that includes sequencing, selection, and iteration, that explains how it works.

Testing - the written response: describes two calls to the selected procedure identified in written response 3c. Each call must pass a different argument(s) that causes a different segment of code in the algorithm to execute. 

