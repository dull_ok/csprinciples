product = 1  # Start with multiplication identity
number = 0
while number < 10:
    number = number + 1
    product = product * number
print(product)
